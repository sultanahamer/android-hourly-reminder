package com.github.axet.hourlyreminder.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.app.AlarmManager;
import com.github.axet.hourlyreminder.app.HourlyApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class EventsListDialog {
    Context context;
    View v;
    RecyclerView list;
    RecyclerView.Adapter adapter;
    AlarmManager am;
    ArrayList<Long> kk;

    TextView time;
    TextView fired;
    TextView delayed;
    TextView charged;
    TextView state;
    TextView type;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView time;
        TextView fired;
        TextView diff;

        public ViewHolder(View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.event_time);
            fired = (TextView) itemView.findViewById(R.id.event_fired);
            diff = (TextView) itemView.findViewById(R.id.event_diff);
        }
    }

    public class Adapter extends RecyclerView.Adapter<ViewHolder> {
        int selected = -1;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.event, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            long k = kk.get(position);

            int color = ThemeUtils.getThemeColor(context, android.R.attr.textColorPrimary);
            holder.fired.setTypeface(null, Typeface.NORMAL);
            holder.fired.setTextColor(color);
            holder.diff.setTypeface(null, Typeface.NORMAL);
            holder.diff.setTextColor(color);

            final AlarmManager.Info info = am.infos.get(k);
            if (info != null) {
                holder.time.setText(AlarmManager.formatTime(info.time));
                if (info.state.equals(AlarmManager.STATE_FIRED)) {
                    holder.fired.setText(AlarmManager.formatTime(info.fired));
                    long diff = info.fired - info.time;
                    holder.diff.setText(HourlyApplication.formatDuration(context, diff));
                    if (diff > info.sanity()) {
                        holder.diff.setTypeface(null, Typeface.BOLD);
                        holder.diff.setTextColor(Color.RED);
                    }
                } else {
                    holder.diff.setText("");
                }
                if (info.state.equals(AlarmManager.STATE_PENDING))
                    holder.fired.setText(R.string.events_pending);
                if (info.state.equals(AlarmManager.STATE_MISSED)) {
                    holder.fired.setText(R.string.events_missed);
                    holder.fired.setTypeface(null, Typeface.BOLD);
                    holder.fired.setTextColor(Color.RED);
                }
            } else {
                holder.fired.setText("UNKNOWN");
                holder.fired.setTypeface(null, Typeface.BOLD);
                holder.fired.setTextColor(Color.RED);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selected = holder.getAdapterPosition();
                    show(info);
                    notifyDataSetChanged();
                }
            });
            if (selected == position)
                holder.itemView.setBackgroundColor(0x33FFFF00);
            else if (info.type.equals(AlarmManager.TYPE_BOOT))
                holder.itemView.setBackgroundColor(0x3300FF00);
            else
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        @Override
        public int getItemCount() {
            return kk.size();
        }
    }

    void show(AlarmManager.Info info) {
        time.setText(AlarmManager.formatTime(info.time));
        if (info.fired != 0) {
            fired.setText(AlarmManager.formatTime(info.fired));
            long diff = info.fired - info.time;
            delayed.setText(HourlyApplication.formatDuration(context, diff));
        } else {
            fired.setText("");
            delayed.setText("");
        }
        charged.setText(info.charge);
        state.setText(info.state);
        type.setText(info.type);
    }

    public EventsListDialog(Context context) {
        this.context = context;
        am = HourlyApplication.from(context).items.am;
        kk = new ArrayList<>(am.infos.keySet());
        Collections.sort(kk, Collections.reverseOrder());
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        v = inflater.inflate(R.layout.eventdialog, null, false);

        time = (TextView) v.findViewById(R.id.time);
        fired = (TextView) v.findViewById(R.id.fired);
        delayed = (TextView) v.findViewById(R.id.delayed);
        type = (TextView) v.findViewById(R.id.type);
        charged = (TextView) v.findViewById(R.id.charged);
        state = (TextView) v.findViewById(R.id.state);
        list = (RecyclerView) v.findViewById(R.id.list);

        time.setText("");
        fired.setText("");
        delayed.setText("");
        charged.setText("");
        state.setText("");
        type.setText("");

        adapter = new Adapter();
        LinearLayoutManager lm = new LinearLayoutManager(context);
        list.addItemDecoration(new DividerItemDecoration(context, lm.getOrientation()));
        list.setLayoutManager(lm);
        list.setAdapter(adapter);
        builder.setView(v);
        builder.setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }
}
