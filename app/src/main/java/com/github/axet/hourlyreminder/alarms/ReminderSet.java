package com.github.axet.hourlyreminder.alarms;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.format.DateFormat;

import com.github.axet.hourlyreminder.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

public class ReminderSet extends WeekSet {
    public static final int TYPE_NOTIFICATION = RingtoneManager.TYPE_NOTIFICATION;
    public static final Uri DEFAULT_NOTIFICATION = RingtoneManager.getDefaultUri(TYPE_NOTIFICATION);

    public static final Set<String> DEF_HOURS = new TreeSet<>(Arrays.asList("08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21"));

    public Set<String> hours; // actual hours selected
    public Reminder.Key start; // start interval
    public Reminder.Key end; // start interval
    public List<Reminder> list; // generated reminders (depend on repeat and hours/interval)
    public int repeat; // minutes, negative means once per hour at specified time
    public long last; // last reminder announced, to prevent double announcements
    public TimeZone timezone; // runtime information

    public static int indexHour(ArrayList<String> list, String val) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).startsWith(val))
                return i;
        }
        return -1;
    }

    public static String format(Context context, Set<String> hours, int repeat) {
        boolean h24 = DateFormat.is24HourFormat(context);

        String AM = context.getString(R.string.day_am);
        String PM = context.getString(R.string.day_pm);

        String H = context.getString(R.string.hour_symbol);

        String str = "";

        ArrayList<String> list = new ArrayList<>(hours);

        // find start index, it maybe mid night or daylight interval.
        int start = 0;
        if (indexHour(list, Reminder.format(0)) != -1 && indexHour(list, Reminder.format(23)) != -1) {
            Reminder.Key p = null;
            for (int prev = list.size() - 1; prev >= 0; prev--) {
                Reminder.Key n = new Reminder.Key(list.get(prev));
                if (p != null && !n.next(p, repeat)) {
                    start = prev + 1;
                    break;
                }
                p = n;
            }
        }

        int count = 0;
        Reminder.Key prev = null;
        Reminder.Key last = null;
        for (int i = 0; i < list.size(); i++) {
            int index = start + i;
            if (index >= list.size())
                index -= list.size();
            Reminder.Key s = new Reminder.Key(list.get(index));
            if (prev != null && prev.next(s, repeat)) {
                count++;
            } else {
                if (count != 0) {
                    if (!h24) {
                        if (last.hour < 12 && prev.hour >= 12)
                            str += AM;
                        if (last.hour >= 12 && s.hour < 12)
                            str += PM;
                    }
                    if (count == 1 && (last.min == 0 && prev.min == 0))
                        str += ",";
                    else
                        str += "-";
                    str += prev.formatShort(context);
                    if (!h24) {
                        if (last.hour < 12 && s.hour >= 12)
                            str += AM;
                        if (last.hour >= 12 && s.hour < 12)
                            str += PM;
                    }
                    str += ",";
                    str += s.formatShort(context);
                    last = s;
                } else {
                    if (last != null) {
                        if (!h24) {
                            if (last.hour < 12 && s.hour >= 12)
                                str += AM;
                            if (last.hour >= 12 && s.hour < 12)
                                str += PM;
                        }
                        str += ",";
                    }
                    str += s.formatShort(context);
                    last = s;
                }
                count = 0;
            }
            prev = s;
        }

        if (count != 0) {
            if (!h24) {
                if (last.hour < 12 && prev.hour >= 12)
                    str += AM;
                if (last.hour >= 12 && prev.hour < 12)
                    str += PM;
            }
            if (count == 1 && (last.min == 0 && prev.min == 0))
                str += ",";
            else
                str += "-";
            str += prev.formatShort(context);
        }
        if (prev != null) {
            if (h24)
                str += H;
            else
                str += (prev.hour >= 12 ? PM : AM);
        }

        return str;
    }

    public ReminderSet(Context context, Set<String> hours, int repeat) {
        super(context);
        this.repeat = repeat;
        this.ringtoneValue = DEFAULT_NOTIFICATION;
        load(hours);
    }

    public ReminderSet(Context context, Reminder.Key s, Reminder.Key e, int repeat) {
        super(context);
        start = s;
        end = e;
        this.repeat = repeat;
        this.ringtoneValue = DEFAULT_NOTIFICATION;
        reload();
    }

    public ReminderSet(Context context, Set<String> hours) {
        super(context);
        this.repeat = 60;
        this.enabled = true;
        this.ringtone = false;
        this.ringtoneValue = DEFAULT_NOTIFICATION;
        load(hours);
    }

    public ReminderSet(Context context) {
        super(context);
        this.beep = true;
        this.speech = true;
        this.ringtone = false;
        this.ringtoneValue = DEFAULT_NOTIFICATION;
        this.repeat = 60;
        load(DEF_HOURS);
    }

    public ReminderSet(Context context, String json) {
        super(context, json);
    }

    public String format() {
        return format(context, hours, repeat);
    }

    @Override
    public String formatDays() {
        if (!weekdaysCheck)
            return context.getString(R.string.Everyday);
        else
            return super.formatDays();
    }

    public void reload() {
        last = 0;
        if (start != null && end != null) {
            hours = new TreeSet<>();
            list = new ArrayList<>();
            list.add(new Reminder(this, start.hour, start.min));
            Date now = new Date();
            Calendar d = Calendar.getInstance();
            d.setTime(now);
            d.set(Calendar.HOUR_OF_DAY, start.hour);
            d.set(Calendar.MINUTE, start.min);
            Calendar e = Calendar.getInstance();
            e.setTime(now);
            e.set(Calendar.HOUR_OF_DAY, end.hour);
            e.set(Calendar.MINUTE, end.min);
            if (d.after(e)) { // trougth mid-night
                e.set(Calendar.HOUR_OF_DAY, 23);
                e.set(Calendar.MINUTE, 59);
                e.set(Calendar.SECOND, 59);
                e.set(Calendar.MILLISECOND, 999);
                while (d.before(e) || d.equals(e)) {
                    Reminder.Key k = new Reminder.Key(d.get(Calendar.HOUR_OF_DAY), d.get(Calendar.MINUTE));
                    list.add(new Reminder(this, k.hour, k.min));
                    hours.add(k.key);
                    d.add(Calendar.MINUTE, repeat);
                }
                e.setTime(d.getTime());
                e.set(Calendar.HOUR_OF_DAY, end.hour);
                e.set(Calendar.MINUTE, end.min);
                while (d.before(e) || d.equals(e)) {
                    Reminder.Key k = new Reminder.Key(d.get(Calendar.HOUR_OF_DAY), d.get(Calendar.MINUTE));
                    list.add(new Reminder(this, k.hour, k.min));
                    hours.add(k.key);
                    d.add(Calendar.MINUTE, repeat);
                }
            } else {
                while (d.before(e) || d.equals(e)) {
                    Reminder.Key k = new Reminder.Key(d.get(Calendar.HOUR_OF_DAY), d.get(Calendar.MINUTE));
                    list.add(new Reminder(this, k.hour, k.min));
                    hours.add(k.key);
                    d.add(Calendar.MINUTE, repeat);
                }
            }
        } else {
            load(hours);
        }
    }

    public void load(ReminderSet rs) {
        if (rs.start != null && rs.end != null) {
            start = rs.start;
            end = rs.end;
            repeat = rs.repeat;
            list = new ArrayList<>(rs.list);
            hours = new TreeSet<>(rs.hours);
        } else {
            start = null;
            end = null;
            repeat = rs.repeat;
            load(rs.hours);
        }
    }

    public void load(Set<String> hours) {
        Calendar cur = Calendar.getInstance();

        this.hours = new TreeSet<>(hours);
        this.list = new ArrayList<>();

        ArrayList<String> list = new ArrayList<>(hours);

        if (list.isEmpty())
            return;

        // find start index, it maybe mid night or daylight interval.
        int start = 0;
        if (list.contains(Reminder.format(0))) {
            for (int prev = 23; prev >= 0; prev--) {
                Reminder.Key hh = new Reminder.Key(prev, Reminder.HALF);
                int i = list.indexOf(hh.key);
                if (i != -1)
                    start = i;
                Reminder.Key h = new Reminder.Key(prev);
                i = list.indexOf(h.key);
                if (i == -1)
                    break;
                start = i;
            }
        }

        Reminder.Key prev = null;
        for (int i = 0; i <= list.size(); i++) {
            int index = start + i;
            if (index >= list.size())
                index -= list.size();
            Reminder.Key s = new Reminder.Key(list.get(index));
            if (prev != null) {
                int max;
                if (prev.next(s, repeat)) { // have next, roll up full hour
                    if (s.min == 0)
                        max = 60; // 59 + 1
                    else
                        max = s.min + 1; // min + 1
                } else {
                    max = repeat;
                }
                add(prev, max);
            }
            prev = s;
        }

        timezone = cur.getTimeZone();
    }

    void add(Reminder.Key prev, int max) {
        if (repeat > 0) {
            for (int m = prev.min; m < max; m += repeat) {
                Reminder r = new Reminder(this, prev.hour, m);
                r.enabled = true;
                list.add(r);
            }
        } else {
            int min = -repeat;
            if (prev.min < min) {
                Reminder r = new Reminder(this, prev.hour, min);
                r.enabled = true;
                list.add(r);
            }
        }
    }

    @Override
    public void setNext() {
        super.setNext();
        reload();
    }

    @Override
    public void load(JSONObject o) throws JSONException {
        super.load(o);
        this.weekdaysCheck = true; // force weekdays to true for reminders
        try {
            this.repeat = o.getInt("repeat");
            this.last = o.optLong("last");
            JSONArray list = o.getJSONArray("list");
            Set<String> hh = new TreeSet<>();
            for (int i = 0; i < list.length(); i++)
                hh.add(list.getString(i));
            load(hh);
            String s = o.optString("start");
            if (s != null && !s.isEmpty())
                start = new Reminder.Key(s);
            String e = o.optString("end");
            if (e != null && !e.isEmpty())
                end = new Reminder.Key(e);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public JSONObject save() {
        try {
            JSONArray list = new JSONArray();
            for (String h : hours)
                list.put(h);
            JSONObject o = super.save();
            o.put("repeat", this.repeat);
            o.put("list", list);
            o.put("last", last);
            if (start != null)
                o.put("start", start.key);
            if (end != null)
                o.put("end", end.key);
            return o;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Uri defaultRingtone() {
        return DEFAULT_NOTIFICATION;
    }

    @Override
    public String toString() {
        return format() + " " + formatDays();
    }
}
