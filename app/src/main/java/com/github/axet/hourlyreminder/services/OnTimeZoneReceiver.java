package com.github.axet.hourlyreminder.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.github.axet.hourlyreminder.app.HourlyApplication;

import java.util.Calendar;

public class OnTimeZoneReceiver extends BroadcastReceiver {
    String TAG = OnTimeZoneReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive " + intent);
        String a = intent.getAction();
        if (a == null)
            return;
        if (a.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
            HourlyApplication app = HourlyApplication.from(context);
            app.items.timezoneSanity(Calendar.getInstance());
            AlarmService.registerNextAlarm(context);
        }
    }
}
