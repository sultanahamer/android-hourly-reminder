package com.github.axet.hourlyreminder.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.activities.MainActivity;
import com.github.axet.hourlyreminder.alarms.Alarm;
import com.github.axet.hourlyreminder.alarms.ReminderSet;
import com.github.axet.hourlyreminder.services.AlarmService;

import org.json.JSONException;
import org.json.JSONObject;

public class ActiveReminder {
    public static final String TAG = ActiveReminder.class.getSimpleName();

    public static final String DISMISS = ActiveReminder.class.getCanonicalName() + ".DISMISS"; // dismiss current alarm action

    HourlyApplication.ItemsStorage items;
    Context context;
    Sound parent; // parent sound, holding 'done'
    Runnable done;
    Sound sound;
    WakeScreen wake;
    public FireReminider reminider;

    OptimizationPreferenceCompat.PersistentIcon icon;

    public static FireReminider getReminder(Intent intent) {
        String json = intent.getStringExtra("reminder");
        if (json == null || json.isEmpty())
            return null;
        return new FireReminider(json);
    }

    public static class FireReminider {
        public long time;
        public Sound.Playlist list;

        public FireReminider(String json) {
            load(json);
        }

        public FireReminider(Sound.Playlist a, long t) {
            time = t;
            list = a;
        }

        public void merge(ReminderSet rs) {
            list.withAlarm(rs);
        }

        public void load(String json) {
            try {
                JSONObject o = new JSONObject(json);
                list = new Sound.Playlist(o);
                time = o.getLong("time");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public JSONObject save() {
            JSONObject o = list.save();
            try {
                o.put("time", time);
                return o;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public String toString() {
            return "Reminder " + AlarmManager.formatTime(time);
        }
    }

    public ActiveReminder(Sound s) {
        parent = s;
        context = s.context;
        sound = new Sound(context);
        items = HourlyApplication.from(context).items;

        icon = new OptimizationPreferenceCompat.PersistentIcon(context, HourlyApplication.NOTIFICATION_REMINDER_ICON) {
            @Override
            public Notification build(Intent intent) {
                FireReminider r = getReminder(intent);

                PendingIntent main = PendingIntent.getActivity(context, 0,
                        new Intent(context, MainActivity.class).putExtra(MainActivity.SHOW_TAB, 0),
                        PendingIntent.FLAG_UPDATE_CURRENT);

                RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notification_alarm);

                builder.setTheme(HourlyApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                        .setChannel(HourlyApplication.from(context).channelReminders)
                        .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                        .setTitle(context.getString(R.string.app_name))
                        .setWhen(icon.notification)
                        .setMainIntent(main)
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setOngoing(false);

                if (r != null) {
                    PendingIntent button = PendingIntent.getService(context, 0,
                            new Intent(context, AlarmService.class).setAction(DISMISS).putExtra("reminder", r.save().toString()),
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setDeleteIntent(button);
                    builder.setOnClickPendingIntent(R.id.notification_button, button);
                    builder.setText(Alarm.format2412(context, r.time));
                }

                return builder.build();
            }
        };
        icon.create();
    }

    public void start(FireReminider r, final Runnable done) {
        this.done = done;
        parent.dones.add(done);
        reminider = r;

        Log.d(TAG, "time=" + Alarm.format24(reminider.time));

        icon.updateIcon(new Intent().putExtra("reminder", reminider.save().toString()));

        SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.getBoolean(HourlyApplication.PREFERENCE_WAKEUP, true)) {
            Log.d(TAG, "Wake screen");
            if (wake == null)
                wake = new WakeScreen(context);
            wake.wake();
        }
        Runnable rr = new Runnable() {
            @Override
            public void run() {
                parent.done(done);
            }
        };
        SoundConfig.Silenced s = sound.playList(reminider.list, reminider.time, rr);
        Sound.silencedToast(context, s, reminider.time);
    }

    public void close() {
        if (wake != null) {
            wake.close();
            wake = null;
        }
        parent.remove(done);
        if (sound != null) {
            sound.close();
            sound = null;
        }
        if (icon != null) {
            icon.close();
            icon = null;
        }
    }
}
