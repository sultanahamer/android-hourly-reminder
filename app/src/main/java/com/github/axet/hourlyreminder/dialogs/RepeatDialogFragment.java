package com.github.axet.hourlyreminder.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.CircularSeekBar;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.fragments.HoursDotFragment;
import com.github.axet.hourlyreminder.fragments.HoursIntFragment;
import com.github.axet.hourlyreminder.fragments.RepeatIntervalFragment;
import com.github.axet.hourlyreminder.fragments.RepeatOnceFragment;

import java.util.HashMap;

public class RepeatDialogFragment extends DialogFragment {
    boolean ok;

    MyPagerAdapter adapter;

    public class MyPagerAdapter extends PagerAdapter {
        HashMap<Fragment, View> map = new HashMap<>();
        RepeatIntervalFragment interval = new RepeatIntervalFragment();
        RepeatOnceFragment once = new RepeatOnceFragment();

        MyPagerAdapter() {
            once.context = getContext();
            interval.context = getContext();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return map.get(object) == view;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v;
            Fragment f;
            switch (position) {
                case 0:
                    f = interval;
                    break;
                case 1:
                    f = once;
                    break;
                default:
                    return null;
            }
            f.setArguments(getArguments());
            f.onCreate(null);
            v = f.onCreateView(LayoutInflater.from(getContext()), container, null);
            container.addView(v);
            f.onAttach(getContext());
            map.put(f, v);
            return f;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(map.get((Fragment) object));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.repeat_time_interval);
                case 1:
                    return getString(R.string.hourly);
            }
            return super.getPageTitle(position);
        }

        public void update() {
        }
    }

    public class Result implements DialogInterface {
        public boolean ok;
        public int index;
        public int mins;

        public Result() {
            index = getArguments().getInt("index");
            ok = RepeatDialogFragment.this.ok;
            if (adapter.once.changed > adapter.interval.changed)
                mins = -adapter.once.mins;
            else
                mins = adapter.interval.mins;
            if (mins == 0 || mins == -60)
                mins = 60;
        }

        @Override
        public void cancel() {
        }

        @Override
        public void dismiss() {
        }
    }

    public RepeatDialogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
        } else {
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.hours_main, null, false);
        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        adapter = new MyPagerAdapter();
        pager.setAdapter(adapter);

        if (getArguments().getInt("mins") < 0)
            pager.setCurrentItem(1);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        final AlertDialog d = new AlertDialog.Builder(getActivity())
                .setNegativeButton(getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ok = true;
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view)
                .create();
        return d;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null; // must be null or Illegal state exception
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) a).onDismiss(new Result());
    }

}
