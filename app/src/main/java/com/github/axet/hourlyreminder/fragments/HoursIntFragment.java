package com.github.axet.hourlyreminder.fragments;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.alarms.Reminder;
import com.github.axet.hourlyreminder.alarms.ReminderSet;
import com.github.axet.hourlyreminder.widgets.RoundCheckbox;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

public class HoursIntFragment extends Fragment {
    private boolean mPreferenceChanged;

    TextView status;
    TextView everyText;
    Button okb;
    CheckedTextView hoursEnd;
    CheckedTextView hoursStart;

    Set<String> values;

    public Reminder.Key start;
    public Reminder.Key end;
    public int repeat;

    public Context context;
    public long changed;

    public HoursIntFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repeat = getArguments().getInt("repeat");
        if (savedInstanceState != null) {
            values = new TreeSet<>(Arrays.asList(savedInstanceState.getStringArray("values")));
            String s = savedInstanceState.getString("start");
            if (s != null && !s.isEmpty())
                start = new Reminder.Key(s);
            String e = savedInstanceState.getString("end");
            if (e != null && !e.isEmpty())
                end = new Reminder.Key(e);
            mPreferenceChanged = savedInstanceState.getBoolean("changed");
        } else {
            values = new TreeSet<>(getArguments().getStringArrayList("hours"));
            String s = getArguments().getString("start");
            if (s != null && !s.isEmpty())
                start = new Reminder.Key(s);
            String e = getArguments().getString("end");
            if (e != null && !e.isEmpty())
                end = new Reminder.Key(e);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        values = save();
        outState.putStringArray("values", values.toArray(new String[]{}));
        outState.putBoolean("changed", mPreferenceChanged);
        if (start != null)
            outState.putString("start", start.key);
        if (end != null)
            outState.putString("end", end.key);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Context context = inflater.getContext();
        final View view = inflater.inflate(R.layout.hoursint, container, false);

        status = (TextView) view.findViewById(R.id.status);

        everyText = (TextView) view.findViewById(R.id.alarm_every_text);

        hoursStart = (CheckedTextView) view.findViewById(R.id.hours_start);
        hoursStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date dd = new Date();
                int h = dd.getHours();
                int m = dd.getMinutes();
                if (start != null) {
                    h = start.hour;
                    m = start.min;
                }
                TimePickerDialog d = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hoursStart.setChecked(true);
                        start = new Reminder.Key(hourOfDay, minute);
                        values = save();
                        update();
                    }
                }, h, m, DateFormat.is24HourFormat(context));
                d.show();
                values = save();
                update();
                changed = System.currentTimeMillis();
            }
        });
        hoursEnd = (CheckedTextView) view.findViewById(R.id.hours_end);
        hoursEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date dd = new Date();
                int h = dd.getHours();
                int m = dd.getMinutes();
                if (end != null) {
                    h = end.hour;
                    m = end.min;
                }
                TimePickerDialog d = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hoursEnd.setChecked(true);
                        end = new Reminder.Key(hourOfDay, minute);
                        values = save();
                        update();
                    }
                }, h, m, DateFormat.is24HourFormat(context));
                d.show();
                values = save();
                update();
                changed = System.currentTimeMillis();
            }
        });

        return view;
    }

    void changed() {
        mPreferenceChanged = true;
        values = save();
        update(); // half hours maybe removed
        values = save(); // save them
        update(); // and update new 'values'
    }

    Set<String> save() {
        Set<String> s = new TreeSet<>();
        if (start != null && end != null) {
            ReminderSet rr = new ReminderSet(context, start, end, repeat);
            rr.reload();
            for (Reminder k : rr.list)
                s.add(new Reminder.Key(k.getHour(), k.getMin()).key);
        }
        return s;
    }

    public void update() {
        if (okb != null)
            okb.setEnabled(!values.isEmpty());
        status.setText(ReminderSet.format(context, values, repeat));
        if (start != null)
            hoursStart.setChecked(true);
        if (end != null)
            hoursEnd.setChecked(true);
        everyText.setText(RemindersFragment.everyText(context, repeat));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        update();
    }
}
