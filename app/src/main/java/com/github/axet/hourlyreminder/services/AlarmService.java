package com.github.axet.hourlyreminder.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.provider.AlarmClock;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.activities.MainActivity;
import com.github.axet.hourlyreminder.alarms.Alarm;
import com.github.axet.hourlyreminder.alarms.Reminder;
import com.github.axet.hourlyreminder.alarms.ReminderSet;
import com.github.axet.hourlyreminder.app.ActiveAlarm;
import com.github.axet.hourlyreminder.app.ActiveReminder;
import com.github.axet.hourlyreminder.app.AlarmManager;
import com.github.axet.hourlyreminder.app.HourlyApplication;
import com.github.axet.hourlyreminder.app.Sound;

import java.util.ArrayList;

/**
 * System Alarm Manager notifies this service to create/stop alarms.
 * <p/>
 * All Alarm notifications clicks routed to this service.
 */
public class AlarmService extends PersistentService implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = AlarmService.class.getSimpleName();

    // show upcoming alarm notification
    public static final String NOTIFICATION = AlarmService.class.getCanonicalName() + ".NOTIFICATION";
    // cancel upcoming alarm (from notification button)
    public static final String CANCEL = HourlyApplication.class.getCanonicalName() + ".CANCEL";
    // sound alarm (broadcast)
    public static final String ALARM = HourlyApplication.class.getCanonicalName() + ".ALARM";
    // sound reminder (broadcast)
    public static final String REMINDER = HourlyApplication.class.getCanonicalName() + ".REMINDER";

    HourlyApplication.ItemsStorage items;
    ActiveAlarm alarm;
    ActiveReminder reminder;
    Sound sound;
    Handler handler = new Handler();
    Runnable stopSelf1 = new Runnable() {
        @Override
        public void run() {
            handler.post(stopSelf2); // on power safe onStartCommand called twice for Notification and REMINDER in seq
        }
    };
    Runnable stopSelf2 = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "stopSelf");
            stopSelf();
        }
    };

    static {
        OptimizationPreferenceCompat.setEventServiceIcon(true);
    }

    public static long registerNext(Context context) { // true - running service, false - stop
        HourlyApplication.ItemsStorage items = HourlyApplication.from(context).items;
        return items.registerNextAlarm();
    }

    public static long registerNextAlarm(Context context) {
        long time = registerNext(context);
        boolean b = OptimizationPreferenceCompat.isPersistent(context, HourlyApplication.PREFERENCE_OPTIMIZATION, time != 0);
        final SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        b |= !shared.getString(HourlyApplication.PREFERENCE_ACTIVE_ALARM, "").isEmpty();
        if (b)
            OptimizationPreferenceCompat.startService(context, new Intent(context, AlarmService.class));
        else
            context.stopService(new Intent(context, AlarmService.class));
        return time;
    }

    public static void startClock(Context context) { // https://stackoverflow.com/questions/3590955
        PackageManager packageManager = context.getPackageManager();
        Intent alarmClockIntent = new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_LAUNCHER);
        alarmClockIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String clockImpls[][] = {
                {"HTC Alarm Clock", "com.htc.android.worldclock", "com.htc.android.worldclock.WorldClockTabControl"},
                {"Standard Alarm Clock", "com.android.deskclock", "com.android.deskclock.AlarmClock"},
                {"Froyo Nexus Alarm Clock", "com.google.android.deskclock", "com.android.deskclock.DeskClock"},
                {"Moto Blur Alarm Clock", "com.motorola.blur.alarmclock", "com.motorola.blur.alarmclock.AlarmClock"},
                {"Samsung Galaxy Clock", "com.sec.android.app.clockpackage", "com.sec.android.app.clockpackage.ClockPackage"},
                {"Sony Ericsson Xperia Z", "com.sonyericsson.organizer", "com.sonyericsson.organizer.Organizer_WorldClock"},
                {"ASUS Tablets", "com.asus.deskclock", "com.asus.deskclock.DeskClock"}

        };

        for (int i = 0; i < clockImpls.length; i++) {
            String packageName = clockImpls[i][1];
            String className = clockImpls[i][2];
            try {
                ComponentName c = new ComponentName(packageName, className);
                packageManager.getActivityInfo(c, PackageManager.GET_META_DATA);
                alarmClockIntent.setComponent(c);
                context.startActivity(alarmClockIntent);
                return;
            } catch (PackageManager.NameNotFoundException ignore) {
            }
        }

        Intent openClockIntent = new Intent(AlarmClock.ACTION_SET_ALARM);
        openClockIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(openClockIntent);
    }

    public static void dismissActiveAlarm(Context context, ActiveAlarm.FireAlarm a) {
        Intent intent = new Intent(context, AlarmService.class).setAction(ActiveAlarm.DISMISS).putExtra("alarm", a.save().toString());
        start(context, intent);
    }

    public AlarmService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HourlyApplication app = HourlyApplication.from(this);
        items = app.items;

        sound = new Sound(this);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        // remove notification from previously crashed service
        NotificationManagerCompat.from(this).cancel(HourlyApplication.NOTIFICATION_UPCOMING_ICON);
        NotificationManagerCompat.from(this).cancel(HourlyApplication.NOTIFICATION_REMINDER_ICON);
    }

    @Override
    public void onCreateOptimization() {
        optimization = new OptimizationPreferenceCompat.ServiceReceiver(this, HourlyApplication.NOTIFICATION_PERSISTENT_ICON, HourlyApplication.PREFERENCE_OPTIMIZATION, HourlyApplication.PREFERENCE_NEXT) {
            @Override
            public Notification build(Intent intent) {
                PendingIntent main = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

                RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Low(context, R.layout.notification_alarm);

                builder.setViewVisibility(R.id.notification_button, View.GONE);

                builder.setTheme(HourlyApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                        .setChannel(HourlyApplication.from(context).channelStatus)
                        .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                        .setTitle(getString(R.string.app_name))
                        .setText(getString(R.string.optimization_alive))
                        .setWhen(icon.notification)
                        .setMainIntent(main)
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setOngoing(true);

                return builder.build();
            }
        };
        optimization.create();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.unregisterOnSharedPreferenceChangeListener(this);

        if (sound != null) {
            sound.close();
            sound = null;
        }

        if (alarm != null) {
            alarm.close();
            alarm = null;
        }

        if (reminder != null) {
            reminder.close();
            reminder = null;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        items.am.update();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onRestartCommand() {
        super.onRestartCommand();

        registerNext();

        checkPendingAlarm();
    }

    void checkPendingAlarm() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        String json = shared.getString(HourlyApplication.PREFERENCE_ACTIVE_ALARM, "");
        if (!json.isEmpty() && alarm == null) {
            ActiveAlarm.FireAlarm a = new ActiveAlarm.FireAlarm(json);
            if (!items.isSnoozed(a)) { // do not restart snoozed alarm
                Log.d(TAG, "Alarm fire TYPE1 " + a.settime); // ALARM TYPE1 (service crashed with active alarm)
                alarm = new ActiveAlarm(sound);
                alarm.start(a, new Runnable() {
                    @Override
                    public void run() {
                        if (alarm != null)
                            alarm.close();
                        alarm = null;
                    }
                });
            }
        }
    }

    @Override
    public void onStartCommand(Intent intent) {
        super.onStartCommand(intent);
        String action = intent.getAction();
        if (action != null) {
            long now = System.currentTimeMillis();
            if (action.equals(ActiveAlarm.DISMISS)) {
                ActiveAlarm.FireAlarm a = ActiveAlarm.getAlarm(intent);
                if (alarm == null) {
                    Log.d(TAG, "Alarm dismiss failed == null " + a.ids);
                } else {
                    alarm.alarm.ids.removeAll(a.ids);
                    Log.d(TAG, "Alarm dismissed " + a.ids);
                    if (alarm.alarm.ids.isEmpty()) {
                        alarm.close();
                        alarm = null;
                        final SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(this);
                        SharedPreferences.Editor edit = shared.edit();
                        edit.remove(HourlyApplication.PREFERENCE_ACTIVE_ALARM);
                        edit.commit();
                    } else {
                        final SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(this);
                        SharedPreferences.Editor edit = shared.edit();
                        edit.putString(HourlyApplication.PREFERENCE_ACTIVE_ALARM, alarm.alarm.save().toString());
                        edit.commit();
                    }
                }
            } else if (action.equals(ActiveReminder.DISMISS)) {
                ActiveReminder.FireReminider r = ActiveReminder.getReminder(intent);
                if (reminder == null) {
                    Log.d(TAG, "Reminder dismiss failed == null " + r.time);
                    NotificationManagerCompat.from(this).cancel(HourlyApplication.NOTIFICATION_REMINDER_ICON); // remove notification from previous crash
                } else if (r.time == reminder.reminider.time) {
                    Log.d(TAG, "Reminder dismissed" + AlarmManager.formatTime(r.time));
                    reminder.close();
                    reminder = null;
                } else {
                    Log.d(TAG, "Reminder DISMISS ignored " + AlarmManager.formatTime(r.time));
                }
            } else if (action.equals(NOTIFICATION)) {
                long time15 = intent.getLongExtra("time15", 0);
                long time = intent.getLongExtra("time", 0);
                items.am.fired(time15, now);
                items.showNotificationUpcoming(time);
                registerNext();
            } else if (action.equals(CANCEL)) {
                long time = intent.getLongExtra("time", 0);
                tomorrow(time); // registerNext()
            } else if (action.equals(ALARM) || action.equals(REMINDER)) {
                long time = intent.getLongExtra("time", 0);
                items.am.fired(time, now);
                soundAlarm(time); // registerNext()
            } else { // unknown action
                registerNext();
            }
        } else {
            checkPendingAlarm();
        }
    }

    // cancel alarm 'time' by set it time for day+1 (same hour:min)
    public void tomorrow(long time) {
        for (Alarm a : items.alarms) {
            if (a.getTime() == time && a.enabled) {
                if (a.weekdaysCheck) {
                    // be safe for another timezone. if we moved we better call setNext().
                    // but here we have to jump over next alarm.
                    a.setTomorrow();
                } else {
                    a.setEnable(false);
                }
                HourlyApplication.toastAlarmSet(this, a);
            }
        }

        for (ReminderSet rr : items.reminders) {
            if (rr.enabled) {
                for (Reminder r : rr.list) {
                    if (r.getTime() == time && r.enabled)
                        r.setTomorrow();
                }
            }
        }

        items.save();
        registerNext();
    }

    public long registerNext() {
        sound.exits.remove(stopSelf1);
        handler.removeCallbacks(stopSelf2);
        long time = registerNext(this);
        boolean b = OptimizationPreferenceCompat.isPersistent(this, HourlyApplication.PREFERENCE_OPTIMIZATION, time != 0);
        if (!b)
            sound.after(stopSelf1);
        return time;
    }

    // alarm come from service call (System Alarm Manager) for specified time
    //
    // we have to check what 'alarms' do we have at specified time (can be reminder + alarm)
    // and act properly.
    public void soundAlarm(final long time) {
        // find hourly reminder + alarm = combine proper sound notification_upcoming (can be merge beep, speech, ringtone)
        //
        // then sound alarm or hourly reminder

        final ArrayList<Runnable> caches = new ArrayList<>();
        final Runnable purge = new Runnable() {
            @Override
            public void run() {
                for (Runnable r : caches)
                    r.run();
                caches.clear();
            }
        };

        ActiveAlarm.FireAlarm falarm = null;
        for (Alarm a : items.alarms) { // here can be two alarms with same time
            if (a.getTime() == time && a.enabled) {
                Log.d(TAG, "Sound Alarm " + Alarm.format24(time));
                if (falarm == null)
                    falarm = new ActiveAlarm.FireAlarm(a);
                else
                    falarm.merge(a);
                if (!a.weekdaysCheck) {
                    // disable alarm after it goes off for non recurring alarms (!a.weekdays)
                    a.setEnable(false);
                } else {
                    // calling setNext is more safe. if this alarm have to fire today we will reset it
                    // to the same time. if it is already past today's time (as we expect) then it will
                    // be set for tomorrow.
                    //
                    // also safe if we moved to another timezone.
                    a.setNext();
                }
            }
        }

        Sound.Playlist rlist = null;
        for (final ReminderSet rr : items.reminders) {
            if (rr.enabled) {
                for (Reminder r : rr.list) {
                    if (r.isSoundAlarm(time) && r.enabled) {
                        // calling setNext is more safe. if this alarm have to fire today we will reset it
                        // to the same time. if it is already past today's time (as we expect) then it will
                        // be set for tomorrow.
                        //
                        // also safe if we moved to another timezone.
                        r.setNext();
                        if (rr.last < time) {
                            rr.last = time;
                            if (falarm == null) { // do not cross alarms
                                if (rlist == null)
                                    rlist = new Sound.Playlist(rr);
                                else
                                    rlist.merge(rr);
                            } else { // merge reminder with alarm
                                falarm.merge(rr);
                            }
                        }
                    }
                }
            }
        }

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);

        if (falarm != null) {
            Log.d(TAG, "Alarm " + AlarmManager.formatTime(time));
            if (alarm != null) {
                ArrayList<Long> ids = new ArrayList<>(falarm.ids);
                for (Long id : new ArrayList<>(alarm.alarm.ids))
                    ids.remove(id);
                if (!ids.isEmpty()) // few new alarms remains, play them
                    AlarmService.showNotificationMissedConf(this, alarm.alarm.settime); // dismiss currently playing alarms after conflict, not time based; snooze = off
                alarm.close();
                Log.d(TAG, "Alarm fire TYPE2 " + falarm.settime); // ALARM TYPE2 (currently playuing alarm replaced with new alarm)
            } else {
                String json = shared.getString(HourlyApplication.PREFERENCE_ACTIVE_ALARM, "");
                if (!json.isEmpty()) { // yep, we are already firing the alarm, show missed
                    ActiveAlarm.FireAlarm f = new ActiveAlarm.FireAlarm(json);
                    ArrayList<Long> ids = new ArrayList<>(f.ids); // old ids
                    for (Long id : new ArrayList<>(falarm.ids)) // new ids
                        ids.remove(id);
                    if (!ids.isEmpty()) {
                        Log.d(TAG, "Alarm fire TYPE3 " + f.settime); // ALARM TYPE3 (active alarm, not fired, replaced with new alarm)
                        AlarmService.showNotificationMissedConf(this, f.settime); // dismiss after conflict, not time based; snooze = off
                    } else {
                        Log.d(TAG, "Alarm fire TYPE4 " + f.settime); // ALARM TYPE4 (start new fresh alarm)
                    }
                }
            }
            final Runnable done = new Runnable() {
                @Override
                public void run() {
                    if (alarm != null)
                        alarm.close();
                    alarm = null;
                }
            };
            alarm = new ActiveAlarm(sound);
            alarm.start(falarm, new Runnable() {
                @Override
                public void run() {
                    handler.post(done);
                    purge.run();
                }
            });
        } else if (rlist != null) {
            Log.d(TAG, "Reminder " + AlarmManager.formatTime(time) + " a=" + rlist.beep + " s=" + rlist.speech + " r=" + rlist.isRingtone());
            if (reminder != null)
                reminder.close();
            final Runnable done = new Runnable() {
                @Override
                public void run() {
                    if (reminder != null) {
                        reminder.close();
                        reminder = null;
                    }
                }
            };
            reminder = new ActiveReminder(sound);
            reminder.start(new ActiveReminder.FireReminider(rlist, time), new Runnable() {
                @Override
                public void run() {
                    handler.post(done);
                    purge.run();
                }
            });
        }

        if (falarm != null || rlist != null)
            items.save();
        else
            Log.d(TAG, "Time ignored: " + AlarmManager.formatTime(time)); // double fire, ignore second alarm

        final long timeNext = registerNext();
        Runnable cache = new Runnable() {
            @Override
            public void run() {
                if (items.isSpeaking(timeNext))
                    sound.cache(timeNext, true);
            }
        };
        caches.add(cache);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "onSharedPreferenceChanged " + key);

        // do not update on pref change for alarms, too slow. use direct call from AlarmFragment
//        if (key.startsWith(HourlyApplication.PREFERENCE_ALARMS_PREFIX)) {
//            alarms = HourlyApplication.loadAlarms(this);
//            registerNext();
//        }

        // reset reminders on special events
        if (key.equals(HourlyApplication.PREFERENCE_ALARM))
            registerNext();
    }

    @SuppressLint("RestrictedApi")
    public static void showNotificationMissedConf(Context context, long settime) {
        NotificationManagerCompat nm = NotificationManagerCompat.from(context);

        if (settime == 0) {
            nm.cancel(HourlyApplication.NOTIFICATION_MISSED_ICON); // not supported
        } else {
            PendingIntent main = PendingIntent.getActivity(context, 0,
                    new Intent(context, MainActivity.class).setAction(MainActivity.SHOW_ALARMS_PAGE).putExtra("time", settime),
                    PendingIntent.FLAG_UPDATE_CURRENT);

            String text = context.getString(R.string.AlarmMissedConflict, Alarm.format2412ap(context, settime));

            RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notification_alarm);

            builder.setViewVisibility(R.id.notification_button, View.GONE);

            builder.setTheme(HourlyApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                    .setChannel(HourlyApplication.from(context).channelAlarms)
                    .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                    .setMainIntent(main)
                    .setTitle(context.getString(R.string.AlarmMissed))
                    .setText(text)
                    .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                    .setSmallIcon(R.drawable.ic_launcher_notification);

            nm.notify("missedconf" + System.currentTimeMillis(), HourlyApplication.NOTIFICATION_MISSED_ICON, builder.build());
        }
    }
}
