package com.github.axet.hourlyreminder.app;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.CacheImagesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class AlarmManager extends com.github.axet.androidlibrary.app.AlarmManager {
    public static int MAX_EVENTS = 200;
    public static int FIRED_SANITY = 30 * 1000; // 30 seconds for exact alarm to goes off
    public static int FIRED_SANITY_NORMAL = 60 * 60 * 1000; // 60 minutes for normal event to goes off

    public static String TYPE_SET = "TYPE_SET"; // AlarmManager.set()
    public static String TYPE_EXACT = "TYPE_EXACT"; // AlarmManager.setExact()
    public static String TYPE_EXACT_EX = "TYPE_EXACT_EX"; // AlarmManager.setExactAndAllowWhileIdle()
    public static String TYPE_ALARM = "TYPE_ALARM"; // AlarmManager.setAlarm()
    public static String TYPE_BOOT = "TYPE_BOOT"; // android.intent.action.BOOT_COMPLETED

    public static String CHARGE_BATTERY = "CHARGE_BATTERY";
    public static String CHARGE_POWER = "CHARGE_POWER";
    public static String CHARGE_OFF = "CHARGE_OFF";

    public static String STATE_PENDING = "STATE_PENDING";
    public static String STATE_FIRED = "STATE_FIRED";
    public static String STATE_MISSED = "STATE_MISSED";

    public HashMap<Long, Info> infos = new HashMap<>();

    public static class Info {
        public long last; // last modified time
        public String type; // set, exact, alarm
        public String charge; // phone charge state (battery, power cable, off)
        public long set; // set time, for detect reboots
        public long time; // alarm set time
        public long fired; // actual fired time
        public String state; // pending, fired, missed
        public Intent intent; // runtime information; for cancel events

        public Info() {
        }

        public Info(long time, String type, Intent intent) {
            this.time = time;
            this.type = type;
            this.intent = intent;
            this.state = STATE_PENDING;
        }

        public Info(JSONObject o) throws JSONException {
            load(o);
        }

        public void load(JSONObject o) throws JSONException {
            this.type = o.getString("type");
            this.last = o.optLong("last");
            this.set = o.optLong("set");
            this.state = o.optString("state");
            this.charge = o.optString("charge");
            this.time = o.getLong("time");
            this.fired = o.getLong("fired");
        }

        public JSONObject save() {
            try {
                JSONObject o = new JSONObject();
                o.put("type", this.type);
                o.put("set", this.set);
                o.put("last", last);
                o.put("state", this.state);
                o.put("charge", this.charge);
                o.put("time", this.time);
                o.put("fired", this.fired);
                return o;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public boolean check() {
            if (state == null)
                return true;
            if (type.equals(TYPE_BOOT))
                return true; // ignore boot errors
            if (state.equals(STATE_MISSED))
                return false;
            if (state.equals(STATE_FIRED) && fired - time > sanity())
                return false;
            return true;
        }

        public long sanity() {
            if (type.equals(AlarmManager.TYPE_BOOT))
                return OptimizationPreferenceCompat.BOOT_DELAY; // boot event
            else if (type.equals(AlarmManager.TYPE_SET))
                return AlarmManager.FIRED_SANITY_NORMAL; // normal type
            else
                return AlarmManager.FIRED_SANITY; // ALARM type
        }

        @Override
        public String toString() {
            return formatTime(time);
        }
    }

    public static class Duration {
        public long start; // start time
        public long end; // end time

        public Duration() {
        }

        public Duration(long s, long e) {
            start = s;
            end = e;
        }

        public Duration(JSONObject o) throws JSONException {
            load(o);
        }

        public void load(JSONObject o) throws JSONException {
            start = o.getLong("start");
            end = o.getLong("end");
        }

        public JSONObject save() {
            try {
                JSONObject o = new JSONObject();
                o.put("start", start);
                o.put("end", end);
                return o;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class History extends ArrayList<Duration> {
        public History() {
        }

        public History(String str) throws JSONException {
            load(new JSONArray(str));
        }

        public int find(long time) {
            for (int i = 0; i < size(); i++) {
                Duration d = get(i);
                if (d.start >= time && time <= d.end)
                    return i;
            }
            return -1;
        }

        public void clearOld() {
            while (size() > MAX_EVENTS)
                remove(0);
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, -CacheImagesAdapter.CACHE_DAYS);
            for (Duration d : new ArrayList<>(this)) {
                if (d.end < c.getTimeInMillis())
                    remove(d); // delete old events
            }
        }

        public void load(JSONArray hh) throws JSONException {
            for (int i = 0; i < hh.length(); i++)
                add(new Duration((JSONObject) hh.get(i)));
        }

        public JSONArray save() {
            JSONArray a = new JSONArray();
            for (Duration d : this)
                a.put(d.save());
            return a;
        }
    }

    public void clearOld() {
        ArrayList<Long> ss = new ArrayList<>(infos.keySet());
        while (ss.size() > MAX_EVENTS) {
            infos.remove(ss.get(0)); // delete max events
            ss.remove(0);
        }
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -CacheImagesAdapter.CACHE_DAYS);
        for (Long k : new ArrayList<>(infos.keySet())) {
            Info info = infos.get(k);
            if (info.last < c.getTimeInMillis())
                infos.remove(k); // delete old events
        }
    }

    public String getPower() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        boolean wlCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS;
        return (usbCharge || acCharge || wlCharge) ? CHARGE_POWER : CHARGE_BATTERY;
    }

    public void missed() { // find missed events
        long now = System.currentTimeMillis();
        long b = OptimizationPreferenceCompat.getBootTime();
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        History boot;
        try {
            boot = new History(shared.getString(HourlyApplication.PREFERENCE_BOOT_HISTORY, "[]"));
        } catch (JSONException e) {
            Log.w(TAG, e);
            boot = new History();
        }
        History power;
        try {
            power = new History(shared.getString(HourlyApplication.PREFERENCE_POWER_HISTORY, "[]"));
        } catch (JSONException e) {
            Log.w(TAG, e);
            power = new History();
        }
        for (Long k : new ArrayList<>(infos.keySet())) {
            Info info = infos.get(k);
            if (info.state.equals(STATE_PENDING)) {
                if (info.type.equals(TYPE_BOOT)) {
                    if (info.fired == 0) {
                        info.state = STATE_MISSED;
                        if (boot.find(info.time) >= 0)
                            info.charge = power.find(info.time) >= 0 ? CHARGE_POWER : CHARGE_BATTERY;
                        else
                            info.charge = CHARGE_OFF;
                    }
                } else {
                    if (info.set < b) {
                        infos.remove(k); // android does not keep alarms after reboot
                    } else if (info.time < now) {
                        info.state = STATE_MISSED;
                        if (boot.find(info.time) >= 0)
                            info.charge = power.find(info.time) >= 0 ? CHARGE_POWER : CHARGE_BATTERY;
                        else
                            info.charge = CHARGE_OFF;
                    }
                }
            }
        }
    }

    public AlarmManager(Context context) {
        super(context);
        loadInfo();
        clearOld();
    }

    @Override
    public void cancel(Intent intent) {
        cancelInfo(intent);
        saveInfo();
        super.cancel(intent);
    }

    public void cancelInfo(Intent intent) {
        String id = checkId(0, intent);
        for (Long k : new ArrayList<>(infos.keySet())) {
            Info info = infos.get(k);
            if (info.intent != null && checkId(0, info.intent).equals(id) && info.state.equals(STATE_PENDING))
                infos.remove(k); // cancel remove
        }
        saveInfo();
    }

    @Override
    public Alarm set(long time, Intent intent) {
        cancelInfo(intent);
        Info info = infos.get(time);
        long now = System.currentTimeMillis();
        if (info == null)
            info = new Info();
        info.set = now;
        info.last = now;
        info.time = time;
        info.type = TYPE_SET;
        info.intent = intent;
        info.state = STATE_PENDING;
        infos.put(time, info);
        saveInfo();
        return super.set(time, intent);
    }

    @Override
    public Alarm setExact(long time, Intent intent) {
        cancelInfo(intent);
        Info info = infos.get(time);
        long now = System.currentTimeMillis();
        if (info == null)
            info = new Info();
        info.set = now;
        info.last = now;
        info.time = time;
        if (Build.VERSION.SDK_INT >= 23)
            info.type = TYPE_EXACT_EX;
        else if (Build.VERSION.SDK_INT >= 19)
            info.type = TYPE_EXACT;
        else
            info.type = TYPE_SET;
        info.intent = intent;
        info.state = STATE_PENDING;
        infos.put(time, info);
        saveInfo();
        return super.setExact(time, intent);
    }

    @Override
    public Alarm setAlarm(long time, Intent intent, long showTime, Intent showIntent) {
        cancelInfo(intent);
        Info info = infos.get(time);
        long now = System.currentTimeMillis();
        if (info == null)
            info = new Info();
        info.set = now;
        info.last = now;
        info.time = time;
        if (Build.VERSION.SDK_INT >= 21)
            info.type = TYPE_ALARM;
        else if (Build.VERSION.SDK_INT >= 19)
            info.type = TYPE_EXACT;
        else
            info.type = TYPE_SET;
        info.intent = intent;
        info.state = STATE_PENDING;
        infos.put(time, info);
        saveInfo();
        return super.setAlarm(time, intent, showTime, showIntent);
    }

    public void fired(long time, long now) {
        Info info = infos.get(time);
        if (info == null)
            return;
        info.last = now;
        info.state = STATE_FIRED;
        info.charge = getPower();
        info.fired = now;
        info.intent = null;
        missed();
        saveInfo();
    }

    public void load(JSONArray a) {
        infos.clear();
        try {
            for (int i = 0; i < a.length(); i++) {
                Info info = new Info((JSONObject) a.get(i));
                infos.put(info.time, info);
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public JSONArray save() {
        JSONArray a = new JSONArray();
        for (Long k : infos.keySet()) {
            Info i = infos.get(k);
            a.put(i.save());
        }
        return a;
    }

    public void loadInfo() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            load(new JSONArray(shared.getString(HourlyApplication.PREFERENCE_EVENTS_HISTORY, "[]")));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveInfo() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        shared.edit().putString(HourlyApplication.PREFERENCE_EVENTS_HISTORY, save().toString()).apply();
    }

    public void setBoot(long time) {
        long now = System.currentTimeMillis();
        long boot = OptimizationPreferenceCompat.getBootTime(); // time when device booted
        Info info = infos.get(boot);
        if (info == null)
            info = new Info();
        info.set = boot;
        info.time = boot;
        info.type = TYPE_BOOT;
        info.fired = time;
        info.charge = getPower();
        info.state = STATE_FIRED;
        info.last = now;
        infos.put(boot, info);
        bootCheck();
        saveInfo();
    }

    public void bootCheck(long time) {
        long now = System.currentTimeMillis();
        long boot = OptimizationPreferenceCompat.getBootTime(); // time when device booted
        Info info = infos.get(boot);
        if (info == null) {
            info = new Info();
            info.state = STATE_PENDING;
        }
        info.set = boot;
        info.time = boot;
        info.type = TYPE_BOOT;
        info.last = now;
        infos.put(boot, info);
        bootCheck();
        saveInfo();
    }

    public void bootCheck() { // fill boot chronology (time when device was on)
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        History boot;
        try {
            boot = new History(shared.getString(HourlyApplication.PREFERENCE_BOOT_HISTORY, "[]"));
        } catch (JSONException e) {
            Log.w(TAG, e);
            boot = new History();
        }
        long now = System.currentTimeMillis();
        long start = OptimizationPreferenceCompat.getBootTime();
        Duration d;
        if (boot.size() == 0) {
            d = new Duration(start, now);
            boot.add(d);
        } else {
            d = boot.get(boot.size() - 1);
            if (d.end < start) {
                d = new Duration(start, now);
                boot.add(d);
            }
            if (d.start > start && start < d.end)
                d.end = now; // update last boot time
        }
        boot.clearOld();
        shared.edit().putString(HourlyApplication.PREFERENCE_BOOT_HISTORY, boot.save().toString()).apply();
    }

    public void powerOn() {
        bootCheck();
        long now = System.currentTimeMillis();
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        History power;
        try {
            power = new History(shared.getString(HourlyApplication.PREFERENCE_POWER_HISTORY, "[]"));
        } catch (JSONException e) {
            Log.w(TAG, e);
            power = new History();
        }
        Duration d = new Duration(now, now);
        power.add(d);
        power.clearOld();
        shared.edit().putString(HourlyApplication.PREFERENCE_POWER_HISTORY, power.save().toString()).apply();
    }

    public void powerOff() {
        bootCheck();
        long now = System.currentTimeMillis();
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        History power;
        try {
            power = new History(shared.getString(HourlyApplication.PREFERENCE_POWER_HISTORY, "[]"));
        } catch (JSONException e) {
            Log.w(TAG, e);
            power = new History();
        }
        if (power.size() == 0) {
            Duration d = new Duration(now, now);
            power.add(d);
        } else {
            Duration d = power.get(power.size() - 1);
            if (d.start == d.end) {
                d.end = now;
            } else {
                d = new Duration(now, now);
                power.add(d);
            }
        }
        power.clearOld();
        shared.edit().putString(HourlyApplication.PREFERENCE_POWER_HISTORY, power.save().toString()).apply();
    }

    public boolean issues() {
        for (Long k : infos.keySet()) {
            Info info = infos.get(k);
            if (!info.check())
                return true;
        }
        return false;
    }
}
