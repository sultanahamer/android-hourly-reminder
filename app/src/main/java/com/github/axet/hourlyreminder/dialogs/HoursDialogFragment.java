package com.github.axet.hourlyreminder.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.alarms.ReminderSet;
import com.github.axet.hourlyreminder.fragments.HoursDotFragment;
import com.github.axet.hourlyreminder.fragments.HoursIntFragment;

import java.util.HashMap;

public class HoursDialogFragment extends DialogFragment {
    private boolean mPreferenceChanged;

    boolean ok;

    MyPagerAdapter adapter;

    public class MyPagerAdapter extends PagerAdapter {
        HashMap<Fragment, View> map = new HashMap<>();
        HoursDotFragment hours = new HoursDotFragment();
        HoursIntFragment interval = new HoursIntFragment();
        Fragment primary;

        MyPagerAdapter() {
            hours.context = getContext();
            interval.context = getContext();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public void startUpdate(ViewGroup container) {
            super.startUpdate(container);
        }

        @Override
        public void finishUpdate(ViewGroup container) {
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return map.get(object) == view;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v;
            Fragment f;
            switch (position) {
                case 0:
                    f = hours;
                    break;
                case 1:
                    f = interval;
                    break;
                default:
                    return null;
            }
            f.setArguments(getArguments());
            f.onCreate(null);
            v = f.onCreateView(LayoutInflater.from(getContext()), container, null);
            container.addView(v);
            f.onAttach(getContext());
            map.put(f, v);
            return f;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            primary = (Fragment) object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(map.get((Fragment) object));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.hours);
                case 1:
                    return getString(R.string.reminders_interval);
            }
            return super.getPageTitle(position);
        }

        public void update() {
        }
    }

    public class Result implements DialogInterface {
        public int index;
        public boolean ok;
        public ReminderSet rs;

        public Result() {
            if (adapter.interval.start != null && adapter.interval.end != null && adapter.interval.changed > adapter.hours.changed)
                rs = new ReminderSet(getActivity(), adapter.interval.start, adapter.interval.end, adapter.interval.repeat);
            else
                rs = new ReminderSet(getActivity(), adapter.hours.values, adapter.hours.repeat);
            this.index = getArguments().getInt("index");
            this.ok = HoursDialogFragment.this.ok;
        }

        @Override
        public void cancel() {
        }

        @Override
        public void dismiss() {
        }
    }

    public HoursDialogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.hours_main, null, false);
        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        adapter = new MyPagerAdapter();
        pager.setAdapter(adapter);

        if (getArguments().containsKey("start") && getArguments().containsKey("end"))
            pager.setCurrentItem(1);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setNegativeButton(getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ok = true;
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view);
        final AlertDialog d = builder.create();
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                adapter.hours.okb = d.getButton(DialogInterface.BUTTON_POSITIVE);
            }
        });
        return d;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null; // must be null or Illegal state exception
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) a).onDismiss(new Result());
    }
}
