package com.github.axet.hourlyreminder.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.hourlyreminder.R;
import com.github.axet.hourlyreminder.alarms.Reminder;
import com.github.axet.hourlyreminder.alarms.ReminderSet;
import com.github.axet.hourlyreminder.widgets.RoundCheckbox;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class HoursDotFragment extends Fragment {
    private boolean mPreferenceChanged;

    View v;
    CheckBox min30;
    View hours60_1;
    View hours30_1;
    View hours60_2;
    View hours30_2;
    TextView status;
    public Button okb;
    public Context context;

    public static int[] ids = new int[]{
            R.id.hours_00,
            R.id.hours_01,
            R.id.hours_02,
            R.id.hours_03,
            R.id.hours_04,
            R.id.hours_05,
            R.id.hours_06,
            R.id.hours_07,
            R.id.hours_08,
            R.id.hours_09,
            R.id.hours_10,
            R.id.hours_11,
            R.id.hours_12,
            R.id.hours_13,
            R.id.hours_14,
            R.id.hours_15,
            R.id.hours_16,
            R.id.hours_17,
            R.id.hours_18,
            R.id.hours_19,
            R.id.hours_20,
            R.id.hours_21,
            R.id.hours_22,
            R.id.hours_23,
    };

    public static int[] ids30 = new int[]{
            R.id.hours_0030,
            R.id.hours_0130,
            R.id.hours_0230,
            R.id.hours_0330,
            R.id.hours_0430,
            R.id.hours_0530,
            R.id.hours_0630,
            R.id.hours_0730,
            R.id.hours_0830,
            R.id.hours_0930,
            R.id.hours_1030,
            R.id.hours_1130,
            R.id.hours_1230,
            R.id.hours_1330,
            R.id.hours_1430,
            R.id.hours_1530,
            R.id.hours_1630,
            R.id.hours_1730,
            R.id.hours_1830,
            R.id.hours_1930,
            R.id.hours_2030,
            R.id.hours_2130,
            R.id.hours_2230,
            R.id.hours_2330,
    };

    public static String[] H12 = new String[]{
            "12",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
    };

    public static String[] H24 = new String[]{
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
    };

    public Set<String> values;
    public long changed;
    public int repeat;

    public HoursDotFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repeat = getArguments().getInt("repeat");
        if (savedInstanceState != null) {
            values = new TreeSet<>(Arrays.asList(savedInstanceState.getStringArray("values")));
            mPreferenceChanged = savedInstanceState.getBoolean("changed");
        } else {
            values = new TreeSet<>(getArguments().getStringArrayList("hours"));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        values = save();
        outState.putStringArray("values", values.toArray(new String[]{}));
        outState.putBoolean("changed", mPreferenceChanged);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Context context = inflater.getContext();
        final View view = inflater.inflate(R.layout.hoursdot, container, false);

        status = (TextView) view.findViewById(R.id.status);

        hours60_1 = view.findViewById(R.id.hours_60_1);
        hours30_1 = view.findViewById(R.id.hours_30_1);
        hours60_2 = view.findViewById(R.id.hours_60_2);
        hours30_2 = view.findViewById(R.id.hours_30_2);

        for (int i = 0; i < ids.length; i++) {
            CheckBox c = (CheckBox) view.findViewById(ids[i]);
            String h = Reminder.format(i);
            boolean b = values.contains(h);
            c.setChecked(b);
            c.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changed();
                }
            });
            if (!DateFormat.is24HourFormat(context))
                c.setText(H12[i]);
            else
                c.setText(H24[i]);
        }
        for (int i = 0; i < ids30.length; i++) {
            final int id = ids30[i];
            CheckBox c = (CheckBox) hours30_1.findViewById(id);
            if (c == null)
                c = (CheckBox) hours30_2.findViewById(id);
            Reminder.Key h = new Reminder.Key(i, Reminder.HALF);
            boolean b = values.contains(h.key);
            c.setChecked(b);
            c.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changed();
                }
            });
        }

        View am = view.findViewById(R.id.hours_am);
        View pm = view.findViewById(R.id.hours_pm);

        if (DateFormat.is24HourFormat(context)) {
            am.setVisibility(View.GONE);
            pm.setVisibility(View.GONE);
        } else {
            am.setVisibility(View.VISIBLE);
            pm.setVisibility(View.VISIBLE);
        }

        boolean b = false;
        for (String hour : values) {
            Reminder.Key k = new Reminder.Key(hour);
            if (k.min != 0)
                b = true;
        }
        min30 = (CheckBox) view.findViewById(R.id.title30);
        min30.setChecked(b);
        min30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });

        v = view;

        return view;
    }

    void changed() {
        changed = System.currentTimeMillis();
        mPreferenceChanged = true;
        values = save();
        update(); // half hours maybe removed
        values = save(); // save them
        update(); // and update new 'values'
    }

    Set<String> save() {
        Set<String> s = new TreeSet<>();
        for (int i = 0; i < ids.length; i++) {
            int id = ids[i];
            CheckBox c = (CheckBox) v.findViewById(id);
            Reminder.Key h = new Reminder.Key(i);
            if (c.isChecked())
                s.add(h.key);
        }
        if (min30.isChecked()) {
            for (int i = 0; i < ids30.length; i++) {
                int id = ids30[i];
                CheckBox c = (CheckBox) hours30_1.findViewById(id);
                if (c == null)
                    c = (CheckBox) hours30_2.findViewById(id);
                Reminder.Key hh = new Reminder.Key(i, Reminder.HALF);
                if (c.isChecked())
                    s.add(hh.key);
            }
        }
        return s;
    }

    void setDot30(int id, boolean dot, int dotV) {
        int accent = ThemeUtils.getThemeColor(context, R.attr.colorAccent);
        View v = hours60_1.findViewById(id);
        if (v == null)
            v = hours60_2.findViewById(id);
        v.setVisibility(dotV);
        if (dot)
            v.setBackgroundColor(accent);
        else
            v.setBackgroundColor(RoundCheckbox.SECOND_BACKGROUND);
    }

    void setBut30(int id, Boolean min, int minV) {
        CheckBox c = (CheckBox) hours30_1.findViewById(id);
        if (c == null)
            c = (CheckBox) hours30_2.findViewById(id);
        c.setVisibility(minV);
        if (min != null)
            c.setChecked(min);
    }

    public void update() {
        if (okb != null)
            okb.setEnabled(!values.isEmpty());
        if (!min30.isChecked()) {
            for (int id : ids30) {
                setDot30(id, false, View.INVISIBLE);
                setBut30(id, false, View.INVISIBLE);
            }
            status.setText(ReminderSet.format(context, save(), repeat));
            return;
        } else {
            for (int id : ids30)
                setBut30(id, null, View.VISIBLE);
        }
        status.setText(ReminderSet.format(context, values, repeat));
        for (int hour = 0; hour < 24; hour++) {
            Reminder.Key half = new Reminder.Key(hour, Reminder.HALF);
            String h = Reminder.format(hour);
            int hh = hour + 1;
            if (hh > 23)
                hh = 0;
            String next = Reminder.format(hh);
            if (values.contains(h)) {
                if (values.contains(next)) {
                    setDot30(ids30[hour], true, View.VISIBLE);
                    setBut30(ids30[hour], false, View.VISIBLE);
                } else {
                    boolean b = values.contains(half.key);
                    setDot30(ids30[hour], b, View.VISIBLE);
                    setBut30(ids30[hour], b, View.VISIBLE);
                }
            } else {
                if (values.contains(next)) {
                    boolean b = values.contains(half.key);
                    setDot30(ids30[hour], b, View.VISIBLE);
                } else {
                    setDot30(ids30[hour], false, View.INVISIBLE);
                }
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        update();
    }
}
